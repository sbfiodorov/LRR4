import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MyList<T>
{
    public T[] objects;

    public MyList()
    {
        this.objects = (T[])new Object[0];
    }

    public MyList(List<T> list)
    {
        this.objects = (T[])list.stream().toArray();
    }



    public void add(T t)
    {
        T[] list = (T[])new Object[objects.length + 1];
        for (int i = 0; i < objects.length; i++)
        {
            list[i] = objects[i];
        }
        list[objects.length] = t;
        objects = list;
    }

    public T get(int index)
    {
        if(index < 0 || index + 1 > objects.length){
            throw new ArrayIndexOutOfBoundsException();
        }
        return objects[index];
    }

    public boolean contains(T t)
    {
        return indexOf(t) != -1;
    }


    public int size()
    {
        return objects.length;
    }

    public int indexOf(T t)
    {
        int index = -1;
        for (int i = 0; i < objects.length; i++)
        {
            if(objects[i].equals(t))
            {
                index = i;
            }
        }
        return index;
    }

    @Override
    public String toString()
    {
        return Arrays.stream(objects).map(Object::toString).collect(Collectors.joining(", ", "[ ", " ]"));
    }
}
