import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

class EdmondsKarpTest
{
    private static final String FILE = "data/input.txt";
    private static final String FILE2 = "data/input2.txt";
    private static EdmondsKarp ek = new EdmondsKarp(3);

    @Test
    void addEdgeTest()
    {
        ek.addEdge(0, 1, 3);
        Assertions.assertEquals(ek.getThroughput()[0][1], 3);
    }

    @Test
    void parseEmptyFileTest() throws IOException
    {
        ArrayList<String> line = (ArrayList<String>) Files.readAllLines(Path.of(FILE2));
        Assertions.assertEquals(line.size(), 0);
    }

    @Test
    void getMaxFlowTest() throws IOException
    {
        MyList<String> line = new MyList<>(Files.readAllLines(Path.of(FILE)));
        EdmondsKarp ek = Main.parse(line);
        Assertions.assertEquals(ek.getMaxFlow(0, 5), 5);
    }
}