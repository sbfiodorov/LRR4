public class Data implements Comparable<Data>
{
    private String start;
    private String end;
    private int data;

    Data(String start, String end, int data)
    {
        this.start = start;
        this.end = end;
        this.data = data;
    }

    public String getStart()
    {
        return start;
    }

    public String getEnd()
    {
        return end;
    }

    public int getData()
    {
        return data;
    }

    @Override
    public String toString()
    {
        return "Data" +
                "{" +
                "start = " + start +
                ", end = " + end +
                ", data = " + data +
                '}';
    }

    @Override
    public int compareTo(Data data)
    {
        return 0;
    }
}
