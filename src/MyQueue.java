public class MyQueue<T>
{
    Item first, last;

    public MyQueue()
    {

    }

    public boolean isEmpty()
    {
        return first == null;
    }

    public void add(T value)
    {
        if(first == null)
        {
            first = new Item(value);
            last = first;
        }
        else
        {
            last.next = new Item(value);
            last = last.next;
        }
    }

    public T peek()
    {
        return first.getValue();
    }

    public T poll()
    {
        T item = first.getValue();
        first = first.next;
        return item;
    }

    class Item
    {
        T value;
        Item next;

        public Item getNext()
        {
            return next;
        }

        public T getValue()
        {
            return value;
        }


        public Item(T value, Item next)
        {
            this.value = value;
            this.next = next;
        }

        public Item(T value)
        {
            this(value, null);
        }
    }
}
