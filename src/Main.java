import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;


public class Main
{
    private static final String FILE = "data/input.txt";
    private static final MyList<String> list = new MyList<>();
    private static final MyList<Data> data = new MyList<>();

    public static void main(String[] args) throws IOException
    {
        MyList<String> rows = new MyList<>(Files.readAllLines(Path.of(FILE)));
        EdmondsKarp ek = parse(rows);
    }

    static EdmondsKarp parse(MyList<String> lines)
    {
        int vertices = getVertices(lines).size();
        EdmondsKarp ek = new EdmondsKarp(vertices);
        long maxFlow = 0;
        for (int i = 0; i < lines.size(); i++)
        {
            String line = lines.get(i);
            String[] fragments = line.split("\\s+");
            data.add(new Data(fragments[0], fragments[1], Integer.parseInt(fragments[2])));
        }

        for(int i = 0; i < data.size(); i++)
        {
            Data dataRow = data.get(i);
            if(!list.contains(dataRow.getStart()))
            {
                list.add(dataRow.getStart());
            }

            if(!list.contains(dataRow.getEnd()))
            {
                list.add(dataRow.getEnd());
            }

            ek.addEdge(list.indexOf(dataRow.getStart()), list.indexOf(dataRow.getEnd()), dataRow.getData());
            maxFlow = ek.getMaxFlow(list.indexOf("S"), list.indexOf("T"));
        }
        System.out.println("Max flow is : " + maxFlow);
        return ek;
    }

    private static MyList<String> getVertices(MyList<String> lines)
    {
        MyList<String> vertices = new MyList<>();

        for(int i = 0; i < lines.size(); i++)
        {
            String line = lines.get(i);
            String[] fragments = line.split("\\s+");
            vertices.add(fragments[0]);
            vertices.add(fragments[1]);
        }

        System.out.println(vertices);
        return vertices;
    }
}