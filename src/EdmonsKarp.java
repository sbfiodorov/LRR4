class EdmondsKarp
{
    private long[][] flow;
    private long[][] throughput;
    private int[] parent;
    private boolean[] visited;
    private int numOfVertices;

    EdmondsKarp(int numOfVertices)
    {
        this.numOfVertices = numOfVertices;
        this.flow = new long[this.numOfVertices][this.numOfVertices];
        this.throughput = new long[this.numOfVertices][this.numOfVertices];
        this.parent = new int[this.numOfVertices];
        this.visited = new boolean[this.numOfVertices];
    }

    long[][] getThroughput()
    {
        return throughput;
    }

    void addEdge(int from, int to, long throughput)
    {
        assert throughput >= 0;
        this.throughput[from][to] += throughput;
    }

    long getMaxFlow(int start, int end)
    {
        while (true)
        {
            final MyQueue<Integer> Queue = new MyQueue<>();
            Queue.add(start);

            for (int i = 0; i < this.numOfVertices; ++i)
                visited[i] = false;
            visited[start] = true;

            boolean check = false;
            int current;
            while (!Queue.isEmpty())
            {
                current = Queue.peek();
                if (current == end)
                {
                    check = true;
                    break;
                }
                Queue.poll();
                for (int i = 0; i < numOfVertices; ++i)
                {
                    if (!visited[i] && throughput[current][i] > flow[current][i])
                    {
                        visited[i] = true;
                        Queue.add(i);
                        parent[i] = current;
                    }
                }
            }
            if (!check)
                break;

            long temp = throughput[parent[end]][end] - flow[parent[end]][end];
            for (int i = end; i != start; i = parent[i])
                temp = Math.min(temp, (throughput[parent[i]][i] - flow[parent[i]][i]));

            for (int i = end; i != start; i = parent[i])
            {
                flow[parent[i]][i] += temp;
                flow[i][parent[i]] -= temp;
            }
        }

        long result = 0;
        for (int i = 0; i < numOfVertices; ++i)
            result += flow[start][i];
        return result;
    }
}